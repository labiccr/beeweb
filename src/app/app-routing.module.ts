import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { DataInfoComponent } from './data-info/data-info.component';


const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'hoteldata', component: DataInfoComponent},
  {path: 'references', component: HomePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
