import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  opc = true;
  route: string;
  constructor(private router: Router, location: Location) {
    
   }

  ngOnInit() {
    this.opa()
  }
  
  opa(){
    setTimeout(()=> {
      console.log(window.location.href.indexOf('/hoteldata'));
      if(window.location.href.indexOf('/hoteldata') < 0){
        document.getElementById('titles').style.opacity = '0';
        
      }else{
        document.getElementById('titles').style.opacity = '1';
      }
    }, 500);
    
    
    
  }

}
