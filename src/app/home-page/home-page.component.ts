import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { VideoplayerComponent } from './videoplayer/videoplayer.component';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  info: any;
  uso1 = false;
  uso2 = false;
  uso3 = false;
  select1 = false;
  select2 = false;
  select3 = false;
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  getSelect(m){
    switch(m){
      case 0:
        this.uso1 = true;
        this.select1 = true;
        this.select2 = false;
        this.select3 = false;
        this.info = 0
        break;
      case 1:
        this.uso2 = true;
        this.select1 = false;
        this.select2 = true;
        this.select3 = false;
        this.info = 1;
        break;
      case 2:
        this.uso3 = true;
        this.select1 = false;
        this.select2 = false;
        this.select3 = true;
        this.info = 2;
        break;
          
        
    }
  }
  getColor(n){
    switch(n){
      case 0:
        return '#50be99';
      case 1:
        return '#f4b04d';
      case 2:
        return '#f47c6b';
    }
  }
  open(): void {
    const dialogRef = this.dialog.open(VideoplayerComponent, {
      width: '80vw',
      height: '80vh'
    });
  }

}
